#define _USE_MATH_DEFINES
#include <iostream>
#include <cmath>
using namespace std;
//done check

int main(void)

{
    int selection;

    cout<<"\n Menu";
    cout<<"\n========";
    cout<<"\n 1. Convert Celsius to Fahrenheit";
    cout<<"\n 2. Convert Fahrenheit to Celsius";
    cout << "\n 3. Calculate Circumference of a circle";
    cout << "\n 4. Calculate Area of a circle";
    cout << "\n 5. Area of Rectangle";
    cout << "\n 6. Area of Triangle (Heron�s Formula)";
    cout << "\n 7. Volume of Cylinder";
    cout << "\n 8. Volume of Cone";
    cout << "\n 9. Quit program";
    cout<<"\n Enter selection: ";

    cin >> selection;
    cout << endl;
    if(selection == 1)
    {
        float fahrenheit, celsius;

        cout << "Enter the temperature in Celsius : ";
        cin >> celsius;
        fahrenheit = (celsius * 9.0) / 5.0 + 32;
        cout << "The temperature in Celsius    : " << celsius << endl;
        cout << "The temperature in Fahrenheit : " << fahrenheit << endl;
    }
    else if(selection == 2)
    {
        float fahren, celsius;

        cout << "Enter the temperature in fahrenheit\n";
        cin >> fahren;

        // convert fahreneheit to celsius
        // Subtract 32, then multiply it by 5, then divide by 9

        celsius = 5 * (fahren - 32) / 9;

        cout << fahren << " Fahrenheit is equal to " << celsius << " Centigrade";

    }
    else if (selection == 3)
    {
        float radius, circumference;
        float PI = 3.14159265359;

        cout << "Enter radius of circle : ";
        cin >> radius;
        while(radius < 1)
        {
            cout << "*  Please enter radius more than 1!  *\n";
            cout << "Enter radius of circle : ";
            cin >> radius;
        }

        // Circumference of Circle = 2 X PI x Radius
        circumference = 2 * PI * radius;
        cout << "Circumference of circle : " << circumference;
    }
    else if (selection == 4)
    {
        float radius, area;
        float PI = 3.14159265359;

        cout << "Enter radius of circle : ";
        cin >> radius;
        while(radius < 1)
        {
            cout << "*  Please enter radius more than 1!  *\n";
            cout << "Enter radius of circle : ";
            cin >> radius;
        }

        // Area of Circle = PI x Radius X Radius
        area = PI * radius * radius;
        cout << "Area of circle : " << area;
    }
    else if (selection == 5)
    {
        int width, lngth, area, peri;
        cout << "\n\n Find the Area and Perimeter of a Rectangle :\n";
        cout << "-------------------------------------------------\n";
        cout << " Input the length of the rectangle : ";
        cin >> lngth;
        while (lngth < 1)
        {
            cout << " Input must more than 1!" << endl;
            cout << " Input the length of the rectangle : ";
            cin >> lngth;
        }
        cout << " Input the width of the rectangle : ";
        cin >> width;
        while (width < 1)
        {
            cout << " Input must more than 1!" << endl;
            cout << " Input the width of the rectangle : ";
            cin >> width;
        }
        area = (lngth * width);
        peri = 2 * (lngth + width);
        cout << " The area of the rectangle is : " << area << endl;
        cout << " The perimeter of the rectangle is : " << peri << endl;
        cout << endl;
        return 0;
    }
    else if (selection == 6)
    {
        float side1, side2, side3, area, s;
        cout << "\n\n Find the area of any triangle using Heron's Formula :\n";
        cout << "----------------------------------------------------------\n";
        cout << " Input the length of 1st side  of the triangle : ";
        cin >> side1;
        while (side1 < 1)
        {
            cout << " Input must more than 1!" << endl;
            cout << " Input the length of 1st side  of the triangle : ";
            cin >> side1;
        }
        cout << " Input the length of 2nd side  of the triangle : ";
        cin >> side2;
        while (side2 < 1)
        {
            cout << " Input must more than 1!" << endl;
            cout << " Input the length of 2nd side  of the triangle : ";
            cin >> side2;
        }
        cout << " Input the length of 3rd side  of the triangle : ";
        cin >> side3;
        while (side3 < 1)
        {
            cout << " Input must more than 1!" << endl;
            cout << " Input the length of 3rd side  of the triangle : ";
            cin >> side3;
        }
        s = (side1 + side2 + side3) / 2;
        area = sqrt(s * (s - side1) * (s - side2) * (s - side3));
        cout << " The area of the triangle is : " << area << endl;
        cout << endl;
        return 0;
    }
    else if (selection == 7)
    {
        float volcy, rad1,hgt;
        cout << "\n\n Calculate the volume of a cylinder :\n";
        cout << "-----------------------------------------\n";
        cout<<" Please Enter the Radius of a cylinder : ";
        cin>>rad1;
        while(rad1<1)
        {
            cout << "\nError! Please value more than 1"<<endl;
            cout<<" Please Enter the Radius of a cylinder : ";
            cin>>rad1;
        }
        cout<<" Please Enter the Height of a cylinder : ";
        cin>>hgt;
        while(hgt<1)
        {
            cout << "\nError! Please value more than 1"<<endl;
            cout<<" Please Enter the Height of a cylinder : ";
            cin>>hgt;
        }
        volcy=(3.14*rad1*rad1*hgt);
        cout<<" The volume of a cylinder is : "<< volcy << endl;
        cout << endl;
    }
    else if (selection == 8)
    {
        float cn_Radius, cn_Height, cn_Volume;

        cout << "\nPlease Enter the Radius of a Cone = ";
        cin >> cn_Radius;
        while(cn_Radius<1)
        {
            cout << "\nError! Please value more than 1"<<endl;
            cout << "\nPlease Enter the Radius of a Cone = ";
            cin >> cn_Radius;
        }

        cout << "\nPlease Enter the Height of a Cone = ";
        cin >> cn_Height;
        while(cn_Height<1)
        {
            cout << "\nError! Please value more than 1"<<endl;
            cout << "\nPlease Enter the Radius of a Cone = ";
            cin >> cn_Height;
        }


        cn_Volume = (1.0/3) * M_PI * cn_Radius * cn_Radius * cn_Height;
        cout << "\nThe Volume of a Cone :  " << cn_Volume;
    }
    else if(selection == 9)
    {
        cout << "Thank you for using this program" << endl;
        return 0;
    }
    else
    {
        cout<<"Invalid selection, please try again later" << endl;
    }
    return 0;
}
